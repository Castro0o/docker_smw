Docker image with SMW based on https://gitlab.com/hcc23/semantic-mediawiki-docker/

# inialize mw without LocalSettings.php
## create image smw:
`docker image build --tag smw .`

## inialize mw WITHOUT LocalSettings.php
Will generate: LocalSettings.php & user in DB

* **comment docker_compose.yml** line `- ./LocalSettings.php:/var/www/html/LocalSettings.php`
* build and start smw + mariadb according to docker-compose.yml `docker-compose up -d`
* Setup MediaWiki at http://localhost:8080
    * set mariadb as database
    * set the same db settings as defined in database_mw.env and **server address:** `mw_mariadb` (name of mariadb container)
* Download and save the `LocalSettings.php` into this folder
* Stop the running setup container and remove it `docker-compose down`
* uncomment docker_compose.yml line `- ./LocalSettings.php:/var/www/html/LocalSettings.php` 
* start container `docker-compose up -d`
* visit the the wiki and login as user http://localhost:8080

# Activate SMW extensions
* `docker-compose stop`
* `echo "enableSemantics();" >> LocalSettings.php`
* `echo "wfLoadExtension( 'PageForms' );" >> LocalSettings.php`
* `docker-compose up -d`
* `docker-compose exec mediawiki php maintenance/update.php`




# inialize mw WITH LocalSettings.php

## Edit details:
* Edit services details in: `.env`
* Edit database details in `database_mw.env` & `LocalSettings.php` `$wgDBtype 
$wgDBserver $wgDBname $wgDBuser $wgDBpassword` 
* Edit `LocalSettings.php` 
    * `$wgSitename`
    * `$wgServer = "http://localhost:8080";` to match the http publishing port for MW container 


## create image smw:
* via script: 
    * **without LocalSetting.php** and db: `./start.sh --new`
    * **with LocalSetting.php** and db in `./db/`: `./start.sh --exist`


* `docker image build --tag smw:latest .`
* `docker-compose up -d`
* `docker-compose exec mediawiki php maintenance/update.php`

## remove containers:
* via script: `./stop.sh`

* `docker container stop mw_smw` 
* `docker container rm mw_smw` 
* `docker container stop mw_mariadb` 
* `docker container rm mw_mariadb` 


# issues

# wish list
* fill LocalSettings.php with values from .env file 