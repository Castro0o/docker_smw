FROM mediawiki:stable


WORKDIR /var/www/html

# install missung stuff and php extensions
RUN apt-get update && apt-get install -y \
    git \
    vim \
    unzip \
    libzip-dev \
    && docker-php-ext-install zip

# SMW install
# install composer
RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer
    
# update mediawiki extensions via composer
COPY composer.local.json .
RUN composer update --no-dev

# PageForms install
RUN git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/PageForms.git ./extensions/PageForms