#!/bin/sh
echo "Stop and rm: $mw_container_name"
docker container stop $mw_container_name
docker container rm $mw_container_name

echo "Stop and rm: $db_container_name"
docker container stop $db_container_name
docker container rm $db_container_name