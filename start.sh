#!/bin/bash
export $(cat .env) # turn variables in .env file into envrioment vars

if [ "$1" == "--new" ]
then
    # create necessary dirs for volumes and make them writable
    mkdir $db_local_dir_volume
    chmod a+rw $db_local_dir_volume -R
    mkdir $mw_local_img_volume
    chmod a+rw $mw_local_img_volume -R
    # build image
    docker image build --tag smw:latest .
    # compose with
    docker-compose -f docker-compose.yml up -d
    echo "New wiki created but REQUIRES configuration"
    echo "Visit localhost:publish_port for creating LocalSettings.php"
    echo -e "Database host:" $db_container_name "\n"
    echo "Once LocalSettings.php is downloaded, move it this dir"
    echo -e "And run: ./stop.sh; ./start --exist \n"
elif [ "$1" == "--exist" ]
then
    docker image build --tag smw:latest .
    docker-compose -f docker-compose.yml -f docker-compose.localsettings.yaml up -d
    docker-compose exec mediawiki php maintenance/update.php
    echo "wiki created with existing configuration: ./LocalSetting.php"
else
    echo -e "\nDeploy Mediawiki container HELP:\n"
    echo -e "run: ./start.sh --exist\nto create the wiki container wiki created with existing configuration ./LocalSetting.php"
    echo -e ""
    echo -e "run: ./start.sh --new \nto create the wiki container wiki created with existing configuration ./LocalSetting.php"
    echo -e ""
fi

#!/bin/sh


